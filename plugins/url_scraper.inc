<?php
/**
 * @file
 * Feeds tamper plugin to fetch content from remote URLS.
 *
 * If the source data is a remote URL,
 * fetch it,
 * scrape it,
 * and use the result as the field value.
 */

$plugin = array(
  'form'     => 'feeds_tamper_url_scraper_form',
  'callback' => 'feeds_tamper_url_scraper_callback',
  'name'     => 'URL Scraper',
  'multi'    => 'skip',
  'category' => 'Other',
);

/**
 * Settings form.
 */
function feeds_tamper_url_scraper_form($importer, $element_key, $settings) {
  $form = array();

  $form['help'] = array(
    '#type' => 'markup',
    '#markup' => t('This plugin expects to be given a full remote URL as a page to fetch. The value of the field being tampered will be populated with data retrieved from there.'),
  );
  $form['xpath'] = array(
    '#type' => 'textfield',
    '#title' => t('XPath pattern'),
    '#default_value' => isset($settings['xpath']) ? $settings['xpath'] : '',
    '#description' => t('Pattern to look for inside the fetchd document.'),
  );
  return $form;
}

/**
 * Fetcher callback.
 */
function feeds_tamper_url_scraper_callback($result, $item_key, $element_key, &$field, $settings, $source) {
  // Seems I can't use Feeds log(). Make noise in watchdog instead.
  $strings = array(
    '%url' => $field,
    '!source' => l(basename($field), $field),
  );

  // Ensure it's a reasonable URL.
  if (!valid_url($field)) {
    watchdog(__FUNCTION__, "Provided URL %url looks invalid. Not attempting to scrape it.", $strings, WATCHDOG_WARNING);
    return;
  }

  watchdog(__FUNCTION__, "Fetching content from remote URL %url to scrape it.", $strings, WATCHDOG_INFO);
  // We need to use reasonable timeouts, respect proxies and redirects...
  $response = drupal_http_request($field);

  if ($response->error) {
    $strings['%code'] = $response->code;
    $strings['%error'] = $response->error;
    // This is a pain, but non-fatal.
    watchdog(__FUNCTION__, "URL fetch from remote %field failed, aborting. Maybe it's not available from the source. %code : %error", $strings, WATCHDOG_ERROR);
    return NULL;
  }

  $content = $response->data;
  watchdog(__FUNCTION__, 'URL fetch from remote !source succeeded.', $strings, WATCHDOG_INFO);

  // Now scrape.
  if (!empty($settings['xpath'])) {
    $elements = NULL;
    $strings['%xpath'] = $settings['xpath'];
    // XML fails often. Often tragically.
    try {
      $doc = new DOMDocument();
      $doc->loadHTML($content);
      $xpath = new DOMXpath($doc);
      $elements = $xpath->query($settings['xpath']);
    }
    catch (Exception $e) {
      $strings['%message'] = $e->getMessage();
      watchdog(__FUNCTION__, 'XML unhappiness when running %xpath over %url : %message', $strings, WATCHDOG_ERROR);
    }

    if (!is_null($elements)) {
      $element = $elements->item(0);
      $content = $doc->saveXML($element);
    }
    else {
      watchdog(__FUNCTION__, 'XPath scrape of the content failed. Leaving raw source as-is.', $strings, WATCHDOG_INFO);
    }
  }

  $field = $content;
}
